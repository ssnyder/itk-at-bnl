"""
1 March 2023
Abraham Tishelman-Charny

The purpose of this python module is to create an interactive session for communicating with devices via serial protocol.

https://pyserial.readthedocs.io/en/latest/shortintro.html

Example usage:
python3 -i OpenSerial.py
> ser.write(b"*IDN?\r\n")
> ser.readline()
> ser.readline()
> ser.close()
"""

import serial 
import struct
path = "/dev/serial/by-id/usb-Linux_4.9.220-2.8.7+g57229263ff65_with_2184000.usb_Gadget_Serial_v2.4-if00"
ser = serial.Serial(path,timeout=1)
ser.write(b"*IDN?\r\n")
print(ser.readline())
print(ser.readline())
print(ser.write(b":READ:CHAN:STAT? (@1)\r\n"))
print(ser.readline())
status = ser.readline()
print("status:",status)
#i = struct.unpack('>H', status[:2])[0]
#i = int.from_bytes(status, byteorder='big')
#i &= 0xFFFF
#print("i:",i)
#status_bits = "{0:08b}".format(status)
#print("status in bits:",status_bits)
ser.close()
