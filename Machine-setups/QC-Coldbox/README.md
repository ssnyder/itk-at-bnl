# Quality control coldbox

The purpose of this directory is to keep track of files specific to the Quality Control (QC) coldbox at BNL. This is the coldbox setup used for evaluating the quality and characterizing the performance of hardware components. This includes running thermal cycling. 

## Notes 

The ifconfig name by default which connects to the pi, if you have a USB-ethernet connection to the front of the machine, is `enp0s20f0u1`.
