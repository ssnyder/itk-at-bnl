import Sensors.TC08.tc08 as tc08
from Sensor import Sensor
import sys, os

# Disable
def blockPrint():
    sys.stdout = open(os.devnull, 'w')

# Restore
def enablePrint():
    sys.stdout = sys.__stdout__

class ThermoCouple(Sensor):
    def  __init__(self,serial_number):
        Sensor.__init__(self)
        self.name="TC08"
        self.unit=tc08.TC08(serial_number)
        self.unit.open_unit()
        self.mains_frequency=50
        self.unit.set_channel(1,"K")
        self.unit.set_channel(2,"K")
        self.unit.set_channel(3,"k")
        self.unit.set_channel(4,"K")
        self.unit.set_channel(5,"K")
        self.unit.set_channel(6,"K")
        self.unit.set_channel(7,"K")
        self.unit.set_channel(8,"T")
        self.data={}


    def __del__(self):
        self.unit.close_unit()
        
    def get_data(self):
        self.unit.read_channels()
        self.data["channel_1"]=self.unit.temp_1
        self.data["channel_2"]=self.unit.temp_2
        self.data["channel_3"]=self.unit.temp_3
        self.data["channel_4"]=self.unit.temp_4
        self.data["channel_5"]=self.unit.temp_5
        self.data["channel_6"]=self.unit.temp_6
        self.data["channel_7"]=self.unit.temp_7
        self.data["channel_8"]=self.unit.temp_8
        return self.data


if __name__=="__main__":
    tc=ThermoCouple("A0043/462")
    print(tc.get_data())

