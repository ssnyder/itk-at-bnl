try:
    import graphyte
except ImportError:
    print("not using graphite")
try:
    from influx import InfluxDB 
except ImportError:
    print("not using influx")

class db_manager:

    def __init__(self,backend="influx",database=''):
        #can be either influx or graphite
        if (backend != 'graphite' and backend != 'influx'):
            raise Exception("should be influx or graphite")
        self.backend=backend
        self.database=database

    def init(self,ip,port=8086):
        if(self.backend=="influx"):
            self.client=InfluxDB(ip+":"+str(port)) 
        if(self.backend=='graphite'):
            graphyte.init(ip)

    def send(self,metric,data):
        if(self.backend=="influx"):
            if not self.database:
                raise Exception("using influx, specify a databse")
            self.client.write(self.database, metric, fields={'value':float(data)}) 
        if(self.backend=='graphite'):
            graphyte.send(metric,float(data))
            
