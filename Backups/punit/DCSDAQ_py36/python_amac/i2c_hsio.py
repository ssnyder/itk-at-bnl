'''
export PYTHONPATH=$SCTDAQ_ROOT/bin/python
'''

import hsio
import testUtils
import time

def byteSwap(word):
    return ((word & 0xff) << 8) + ((word>>8) & 0xff)



class i2c_hsio:
    def __init__(self,address,speed=0x00010):
        self.AMAC_SPEED=speed
        self.address=address
        testUtils.initHsio("file", "/tmp/hsioAmac.fromHsio" , "/tmp/hsioAmac.toHsio")
        
    def write(self,register,data):
        ocd=[]
        ocd.append(0x0000+self.AMAC_SPEED+3)
        ocd.append(0x0c00+(self.address << 1))
        ocd.append(0x0400+regid)
        ocd.append(0x1400+data)
        dco=[testUtils.byteSwap(o) for o in ocd]
        hsio.opcode(0x0080,0x0112,dco)
        time.sleep(1)
        ops=testUtils.receivePackets()
        return ops

    def read(self,register,data):
        ocd=[]
        ocd.append(0x0000+self.AMAC_SPEED+3)
        ocd.append(0x0c00+(self.address << 1))
        ocd.append(0x0400+regid)
        ocd.append(0x0c01+(self.address << 1))
        ocd.append(0x1200)
        dco=[testUtils.byteSwap(o) for o in ocd]
        hsio.opcode(0x0080,0x0112,dco)
        time.sleep(1)
        ops=testUtils.receivePackets()
        return ops[1][1][1] & 0xff
