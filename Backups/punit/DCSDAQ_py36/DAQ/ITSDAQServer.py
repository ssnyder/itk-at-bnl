import subprocess
from subprocess import Popen, PIPE
import time
import os
import zmq
import time
import sys
import threading
import graphyte
import ITSDAQLib as ITSDAQ
from zmq import ZMQError
from zmq_client import Client
try:
    import queue
except ImportError:
    import Queue as queue
try:
    import configparser
except ImportError:
    import ConfigParser as configparser

parser = configparser.ConfigParser()
parser.read("DAQConfig.ini")
location=parser['DCSServer']['location']
lock=threading.Lock()
beat=threading.Event()
beat.set()

def heartbeat():
    dcsClient=Client(location,"5550")
    print("started heart beat")
    while beat.is_set():
        with lock:
            dcsClient.SendServerMessage("daqheartbeat")
        print("sent heart beat")
        time.sleep(10)
        
class ServerThread(threading.Thread):
    def __init__(self,threadID):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.lock = threading.Lock()
        self.comm_queue=queue.Queue()
        self.sensor_thread=ModuleMonitor('10.2.241.143',self.comm_queue)
        self.dcsClient=Client(location,"5550")
    def run(self):
        print("Starting " + str(self.threadID))
        self.server()

    def server(self):
        port = "5555"
        context = zmq.Context()
        socket=context.socket(zmq.REP)
        socket.bind("tcp://*:%s" %port)
        heart_beat_thread=threading.Thread(target=heartbeat)
        heart_beat_thread.daemon=True
        while True:
            #start heartbeat thread:
            heart_beat_thread.start()
            message = str(socket.recv())
            print( "recieved message: ",message)
            try:
                ITSDAQ.ClosePipe()
            except :
                pass
            if "break" in message.lower():
                socket.send("Goodbye!".encode())
                self.dcsClient.SendServerMessage("break")
                beat.clear()
                try:
                    ITSDAQ.KillRoot()
                except:
                    pass
                try:
                    ITSDAQ.ClosePipe()
                except:
                    pass
                break

            elif 'sensor' in message.lower():
                data=message.split(" ")
                if('on' in data):
                    self.comm_queue.queue.clear()
                    print("turned on sensors")
                    try:
                        self.sensor_thread.start()
                        socket.send("started module sensor thread".encode())
                    except RuntimeError:
                        pass
                        socket.send("sensor thread already on".encode())
                else:
                    print("turned off sensor")
                    self.comm_queue.put('break')
                    socket.send("stopped module sensor thread".encode())
            elif "start_itsdaq" in message.lower():
                ITSDAQ.start_itsdaq_session()
                socket.send("started an itsdaq session")
            elif "run_test" in message.lower():
                #test message should  be like: run_test True True False...
                data=message.split(" ")[1:]
                test_data=[bl.lower()=='true' for bl in data] #convert fromm array of strings to array of bools
                print("running tests",test_data)
                try:
                    self.comm_queue.put('break')
                    time.sleep(2)
                    test_thread=threading.Thread(target=ITSDAQ.RunTests,args=(test_data,lock,))
                    test_thread.daemon=True
                    test_thread.start()
                    socket.send("Running  tests".encode())
                except ZMQError:
                    print("zmqerror: is the pipe open?")
                    socket.send("itsdaq failed, is pipe open?".encode())                   
            elif "power" in message.lower():
                data=message.split(" ")[1:]
                try:
                    if('on' in data):
                        ITSDAQ.StartAmac()
                        socket.send("started amac".encode())
                    elif('off' in data):
                        ITSDAQ.StopAmac()
                        socket.send("started amac".encode())
                    else:
                        socket.send("did nothing".encode())
                except ZMQError:
                    print("zmqerror: is the pipe open?")
                    socket.send("itsdaq failed, is pipe open?".encode())

            else:
                socket.send("Good to go!".encode())

class ModuleMonitor(threading.Thread):
     def __init__(self,grafana_ip,comm_queue):
         threading.Thread.__init__(self)
         self.comm_queue=comm_queue
         self.grafana_ip=grafana_ip

     def run(self):
         self.sensors()

     def sensors(self):
         graphyte.init(self.grafana_ip)
         command=''
         while True:
            if (not self.comm_queue.empty()):
                command=str(self.comm_queue.get())
                if 'break' in command.lower():
                    print("breaking sensor thread")
                    break
            try:
                with lock:
                    ntc_data=ITSDAQ.read_hybrid_ntc()
                    print("hybrid NTC data",ntc_data)
                    graphyte.send("QCBox.left_hybrid_ntc",float(ntc_data[0]))
                    graphyte.send("QCBox.right_hybrid_ntc",float(ntc_data[1]))
            except Exception as e:
                print("hybrid ntc data error")
                print(e)
            time.sleep(.01)            
            try:
                with lock():
                    amac_ntc=ITSDAQ.read_pb_ntc()
                    print("amac ntc data",amac_ntc)
                    graphyte.send("QCBox.power_board_ntc",float(amac_ntc))
            except Exception as e:
                print("powerboard ntc failure")
                print(e)
            time.sleep(2)
        

#sensor_thread=ModuleMonitor("0.0.0.0",queue.Queue())
#sensor_thread.start()
Thread=ServerThread("ITS server")
Thread.start()
