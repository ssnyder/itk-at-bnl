# ITSDAQ failed to read from DAQ board

If you receive an error from ITSDAQ like this:

```
Read of status block failed!
Failed to read from DAQ board to get initial status, closing connection
```

It means there is an issue reading from your DAQ board (for example, a NEXYS). Sometimes, a fix for this is to power cycle the DAQ board with the physical switch on the board. For a given NEXYS, it can be seen by the red light here:

![NEXYS_Button](Documents/Images/NEXYS_Button.jpg)
<br />
<br />
<br />
For reference, here is what a full NEXYS board may look like: 
<br />
<br />
<br />
![NEXYS_Full](Documents/Images/NEXYS_Full.jpg)
<br />
<br />
<br />
And when connected to an FMC-0514-DP:
<br />
<br />
<br />
![NEXYS_with_FMC-0514-DP](Documents/Images/NEXYS_with_FMC-0514-DP.jpg)
