# ITk at BNL

The purpose of this website is to store documentation, code, and procedures for performing ITk related tasks at BNL.

Relevant documentation and links:

- [ITk Strips ColdJig Wiki](https://gitlab.cern.ch/groups/ColdJigDCS/-/wikis/home)
- [ColdBox CERNBox](https://cernbox.cern.ch/index.php/apps/files/?dir=/__myshares/ITK_ColdBox%20(id%3A240536)&)
- [ColdBox SharePoint (Currently being migrated to above GitLab Wiki)](https://espace.cern.ch/ITkColdBox/_layouts/15/start.aspx#/SitePages/Home.aspx)
- [ITSDAQ documentation](https://atlas-strips-itsdaq.web.cern.ch/)
- [Endcap documentation (has nice README for setting up)](https://gitlab.cern.ch/pekman/endcap_setup/-/blob/2058720a47ad274b575a30fbd35a2a45df945839/README.md)
- [ABCStarHybridModuleTests Twiki](https://twiki.cern.ch/twiki/bin/viewauth/Atlas/ABCStarHybridModuleTests)
- [ITk strips Twiki](https://twiki.cern.ch/twiki/bin/view/Atlas/ITkStrips)
- [BNL electrical results](https://gitlab.cern.ch/fcapocas/bnl-electrical-results)

## Software repositories

* [atlas-itk-strips-daq](https://gitlab.cern.ch/atlas-itk-strips-daq)
     * [itsdaq-sw](https://gitlab.cern.ch/atlas-itk-strips-daq/itsdaq-sw) &rarr; Software for data acquisition of ITk strips modules
     * [itsdaq-fw](https://gitlab.cern.ch/atlas-itk-strips-daq/itsdaq-fw) &rarr; ITSDAQ firmware
* [ColdJigDCS](https://gitlab.cern.ch/ColdJigDCS)
     * [ColdJigLib2](https://gitlab.cern.ch/ColdJigDCS/coldjiglib2) &rarr; Software used to read/set/monitor hardware
     * [Coldbox_controller_WebGUI](https://gitlab.cern.ch/ColdJigDCS/coldbox_controller_webgui) &rarr; Software for producing web GUI to monitor and control hardware
* [atlas-itk](https://gitlab.cern.ch/atlas-itk)
* [ITk production database scripts](https://gitlab.cern.ch/atlas-itk/sw/db/production_database_scripts)
* [ITSDAQ-merger](https://gitlab.cern.ch/mghani/itsdaq-merger/) &rarr; Scripts used to merge ITSDAQ and coldjig jsons, and upload to DB. To eventually be integrated into ITSDAQ.
     * [Related slides](https://indico.cern.ch/event/1191858/contributions/5078438/attachments/2521798/4336652/ITSDAQ_ColdJig_Merger.pdf)
* [YARR](https://gitlab.cern.ch/YARR/YARR) &rarr; The software used for system tests (multiple staves) at SR1 at CERN
* [Possibly useful SHT software](https://github.com/jothanna/sht85/blob/master/sht85/__init__.py)
