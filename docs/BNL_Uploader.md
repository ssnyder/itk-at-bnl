# BNL uploader

The purpose of the ITk production database is to store data related to many things, including module tests etc. At BNL, an uploader has been created in order to avoid the requirement of manual uploading to the DB. The link to this uploader is below:

[https://bnl-uploader.app.cern.ch/](https://bnl-uploader.app.cern.ch/)
