# IV Curves

The purpose of this directory is to contain all things IV-related. Includes a basic plotter for plotting an IV from an ITSDAQ JSON. Example usage:

```
python3 IV_Curves.py --inFile "data/20USBML1234791_R5M1_HALFMODULE_20230308_146_1_MODULE_IV_AMAC.json" --IV_Type module
python3 IV_Curves.py --inFile "data/20USBML1234791_R5M1_HALFMODULE_20230308_146_1_MODULE_IV_AMAC.json" --IV_Type module --ol /my/output/location
```

example output:

![Example IV curve](docs/IV_Curves/plots/20USBML1234791_IVCurve1024_1.jpg)

<img src="/plots/20USBML1234791_IVCurve1024_1.jpg" alt="Example IV curve">

Produce IV plot using ITSDAQ json output for all files in a directory:

```
for f in ./PPB2/*; do python3 IV_Curves.py --inFile ${f} --IV_Type module; done
```
