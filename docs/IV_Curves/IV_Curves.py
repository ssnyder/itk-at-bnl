"""
9 March 2023
Abraham Tishelman-Charny

The purpose of this python module is to plot IV curves. To later be integrated in more general ITk plotter.

Example usage:
python3 IV_Curves.py --inFile data_bypassAMAC/SCIPP-PPB_LS-015_RoomTemp_IV_bypassAMAC.json --IV_Type module --ol .
for f in ./data_bypassAMAC/*; do python3 IV_Curves.py --inFile ${f} --IV_Type module --ol plots_bypassAMAC; done
"""

# imports 
import json 
import pandas as pd 
import numpy as np 
import os 
import csv 
import matplotlib 
matplotlib.use('Agg') # turn off interactive mode
import matplotlib.pyplot as plt

import mplhep as hep 
plt.style.use(hep.style.ATLAS) # customization: https://mplhep.readthedocs.io/en/latest/api.html
from python.SetOptions import options 

# Command line flag options 
inFile = options.inFile
IV_Type = options.IV_Type
ol = options.ol 
debug = options.debug

# Create output directory if it doesn't already exist
if(not os.path.exists(ol)):
    print("Creating output directory:",ol)
    os.system("mkdir -p %s"%(ol))
    os.system("cp %s/../index.php %s"%(ol, ol)) # assumes one directory up from 'ol' exists

# Functions 
def Save_Figures(fig, fileTypes_, OUTNAME_, plotType):
    for fileType in fileTypes_:
        outname = "%s_%s.%s"%(OUTNAME_, plotType, fileType)
        print("")
        print("Saving figure:",outname)
        print("")
        fig.savefig(outname)
    plt.close()    

def Make_IV_Plot(VOLTAGE_vals_, CURRENT_vals_, CURRENT_RMS_vals_, PS_CURRENT_vals_, x_label_, y_label_, OUTNAME_, component_, TEMPERATURE_, debug, include_PSinfo_, bypassAMAC_):
    if(debug): print("CURRENT_RMS_vals_:",CURRENT_RMS_vals_)

    # Subtract the y intercept from all values (offset)
    val_0 = float(CURRENT_vals_[0])
    CURRENT_vals_ = [float(val_i - val_0) for val_i in CURRENT_vals_]

    # params 
    fileTypes = ["png", "pdf"]
    Nentries = len(CURRENT_vals_)
    ymaxIncrement = 0.425

    # IV curve plot 
    fig, ax = plt.subplots(1, figsize=(6, 4), dpi=100)
    hep.atlas.text("ITk Strips Internal", loc=0)

    if(include_PSinfo_): PS_ax = ax.twinx()
    if(bypassAMAC_): AMAC_data_color = "black"
    else: AMAC_data_color = "C0"
    PS_data_color = "C1"

    ax.errorbar(VOLTAGE_vals_, CURRENT_vals_, yerr=CURRENT_RMS_vals_, xerr=None, marker='o', markersize=2, elinewidth=1, linewidth=0.5, color = AMAC_data_color)
    ax.set_xlabel(x_label_)
    ax.set_ylabel(y_label_, color=AMAC_data_color)
    ax.spines['left'].set_color(AMAC_data_color)
    if(include_PSinfo_): ax.spines['right'].set_color(PS_data_color)
    ax.yaxis.label.set_color(AMAC_data_color)
    ax.tick_params(axis='y', which='both', colors=AMAC_data_color) # both = major and minor

    # plot power supply current on same plot w/ different y-axis
    if(debug):
        print("VOLTAGE_vals_",VOLTAGE_vals_)
        print("PS_CURRENT_vals_",PS_CURRENT_vals_)
    
    if(include_PSinfo_):
        PS_ax.plot(VOLTAGE_vals_, PS_CURRENT_vals_, color = PS_data_color, marker='o', markersize=2, linewidth=0.5)
        PS_ax.set_ylabel(r"PS current [${\mu}$A]", color=PS_data_color)
        PS_ax.spines['left'].set_color(AMAC_data_color)
        PS_ax.spines['right'].set_color(PS_data_color)
        PS_ax.yaxis.label.set_color(PS_data_color)
        PS_ax.tick_params(axis='y', which='both', colors=PS_data_color) # both = major and minor

    if(include_PSinfo_): PS_ax.spines['top'].set_color('black')
    ax.spines['top'].set_color('black')
    if(include_PSinfo_): PS_ax.spines['bottom'].set_color('black')
    ax.spines['bottom'].set_color('black')

    # set ymax higher to contain plot labels
    ymin, ymax = ax.get_ylim()
    Incremented_Ymax = ymax + (ymax-ymin)*ymaxIncrement
    ax.set_ylim(ymin, Incremented_Ymax)

    upperText = "%s"%(component_)

    if(type(TEMPERATURE_) == str): TEMPERATURE_ = TEMPERATURE_.replace("_", " ")
    
    plt.text(
        0.94,
        0.95,
        "\n".join([
            upperText,
            "Module IV curve",
            f"{TEMPERATURE_} \N{DEGREE SIGN}C"
        ]
        ),
        horizontalalignment='right',
        verticalalignment='top',
        fontweight = 'bold',
        transform=ax.transAxes
    )

    if(include_PSinfo_): PS_ax.ticklabel_format(style='plain')
    ax.ticklabel_format(style='plain')
    fig.tight_layout()
    Save_Figures(fig, fileTypes, OUTNAME_, "IVCurve")    

fileTypes = ["png", "pdf"]
print("in file:",inFile)
f = open(inFile)
data = json.load(f)
if(debug): print("data:",data)

component = data["component"]
TEMPERATURE = data["results"]["TEMPERATURE"]
if(debug):
    print("component:",component)
    print("TEMPERATURE:",TEMPERATURE)

dataset_names = [
            "CURRENT", "CURRENT_RMS", # from AMAC (or bypass)
            "PS_CURRENT", "VOLTAGE" # from PS
          ]

for dataset_name in dataset_names:
     exec("{}_vals = data['results']['{}']".format(dataset_name, dataset_name))
     if(debug):
         exec("print('{}_vals:',{}_vals)".format(dataset_name, dataset_name))

xlabel, ylabel = "Voltage [V]", "AMAC Current [nA]"
OUTNAME = "{}/{}_{}degC".format(ol, component, TEMPERATURE)

# params
include_PSinfo = 1
absval = 1
bypassAMAC = 0
if("bypass" in OUTNAME): 
    bypassAMAC = 1
    absval = 1
    include_PSinfo = 0
if(bypassAMAC): ylabel = "Sensor Current [nA]"
if(absval):
    CURRENT_vals = [abs(i) for i in CURRENT_vals]

Make_IV_Plot(VOLTAGE_vals, CURRENT_vals, CURRENT_RMS_vals, PS_CURRENT_vals, xlabel, ylabel, OUTNAME, component, TEMPERATURE, debug, include_PSinfo, bypassAMAC) # include component as text on plot 
