# Reception test

When a module is received from another institute, we need to ensure it survived the shipping process properly. We do this by performing a reception test, defined as an electrical test a room temperature.