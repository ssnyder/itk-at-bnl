# Remote connection 

As much as we love being in the lab, sometimes it's nice to be able to connect to our clean room PCs and setups from the comfort of our office or home. These instructions explain how to do so. 

To start, one needs to have an account on the ITk clean room gateway machine located in the testing clean room (ask `throwe@bnl.gov`). After obtaining a username, one should add the following to their `.ssh/config` file from the machine they wish to ssh from:

```
Host cleanroom_gateway
Hostname 130.199.20.1
User <username>
LocalForward 3389 130.199.47.110:3389
```

Then ssh with: `ssh cleanroom_gateway` - ***note*** that if you are not connected to the BNL network via an ethernet cable, you will need to switch on the BNL VPN before proceeding. 

One can then ssh into a PC in the testing clean room, such as the qcbox PC with: `ssh qcbox@130.199.47.110`. Alternatively, one can view the PC browser by:
* Installing `Microsoft Remote Desktop`: ([Apple](https://apps.apple.com/us/app/microsoft-remote-desktop/id1295203466?mt=12))
* Create a new PC with parameters:
  * `PC name: 127.0.0.1`
  * `Friendly name: qcbox`

One should then be able to double click on this new PC within the application, and log in with the qcbox address (qcbox@130.199.47.110) and password (ask Abe if you need it). 

If everything worked, now you should be able to control the qcbox PC from the comfort of your office or home. 

# Connection to stave DCS setups

More information, probably similar but including it here just in case:

Vim `.ssh/config`:

```
Host stave_dcs
Hostname 130.199.20.1
User <yourUsername>
LocalForward 3389 130.199.47.108:3389
```

Commands to enter after that setup:

```bash
ssh stave_dcs 
your password
ssh receptionstation@130.199.47.108
password: bnlphysics
ssh pi@10.2.252.213
password: 0000
```

# Debugging

```bash
sudo systemctl enable xrdp
sudo systemctl start xrdp
sudo systemctl stop firewalld
sudo firewall-cmd --add-port=3389/tcp --permanent
```
