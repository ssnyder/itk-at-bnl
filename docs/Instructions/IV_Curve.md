# IV curve instructions

To take a module IV curve via the AMAC, first connect the module you want to take an IV for to a chuck in the coldbox. Connect LV/HV lines, data and AMAC lines.

* Close lid 
* Turn on chiller, set to +20C 
* Edit the config file `<SCTDAQ_VAR>/config/st_system_config.dat` accordingly, for example if you have one LS module with local ID `BNL-PPB2-MLS-206` in the coldbox:

```
Module    0  1  1   0   0   -1  50  50   BNL-PPB2-MLS-206 Star_Test
```

* Make sure the institution is set in your setup file. You should see a line like `export ITSDAQ_DB_INSTITUTE=BNL`.

* Edit the `<SCTDAQ_VAR>/config/power_supplies.json` accordingly (where $SCTDAQ_VAR is an environment variable you can define in your `~/.bashrc` file, for example with `export SCTDAQ_VAR=/my/itsdaq/directory/`). Then set your config to control a single HV channel, for example:

```
{
    "lv_supplies": [
        {
            "psName" : "/dev/serial/by-id/usb-FTDI_Chipi-X_FT2VC3J3-if00-port0",
            "modName": "BNL Module 0 LV",
            "channel" : 1
        },
        {
            "psName" : "/dev/serial/by-id/usb-FTDI_Chipi-X_FT2VC3J3-if00-port0",
            "modName": "BNL Module 1 LV",
            "channel" : 2
        },
        {
            "psName" : "/dev/serial/by-id/usb-FTDI_Chipi-X_FT2VBPRE-if00-port0",
            "modName": "BNL Module 2 LV",
            "channel" : 1
        },
        {
            "psName" : "/dev/serial/by-id/usb-FTDI_Chipi-X_FT2VBPRE-if00-port0",
            "modName": "BNL Module 3 LV",
            "channel" : 2
        }
    ],

    "hv_supplies": [
        {
            "psName": "/dev/serial/by-id/usb-Linux_4.9.220-2.8.7+g57229263ff65_with_2184000.usb_Gadget_Serial_v2.4-if00",
            "modName": "BNL Module 0 HV",
            "output": 1,
            "deviceType" : "ISEG",
            "channel" : 0
        }
    ]
}

```

In this example, all 4 LV channels will be detected by ITSDAQ, but only the HV PS channel 0, which will be used for the IV.

* Start ITSDAQ in a terminal (`cd itsdaq-sw`, `rid`)
* `loadmacro("SetupModules")`
* To setup just the module in the leftmost chuck: `SetupModules(1,"0")`
* Switch on LV. One way to do this is via the ITSDAQ GUI, only if the LV supplies were defined in the `power_supplies.json`. To turn on an LV channel, one would go to `DCS` in the dropdown menu and select `LV On (One Channel)`...

<img src="/Images/LV_ON_ITSDAQ_GUI.png" alt="Turn on an LV channel with the ITSDAQ GUI">

* In ITSDAQ GUI, select `Module (AMAC) IV scan (550V)` from `DCS` dropdown, and fill out accordingly:

<img src="/Images/IV_config.png" alt="ITSDAQ IV configuration">

Make sure to use the serial number and not local identifier so that the output file has the information necessary for upload.

Click `OK` twice, scan should begin.
