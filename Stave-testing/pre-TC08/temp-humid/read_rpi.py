import time
from serial import Serial


class rpi:
    def __init__(self,location="/dev/ttyUSB0"):
        self.uart=Serial(location,115200,timeout=5)

    def get_data(self):
        data=self.uart.readline()
        print(data)
        data=data.decode().rstrip()
        if not data or 'abcstar' not in data.lower():
            return {"chip":None,"Temperature":None,"Humidity":None}
        else:
            try:
                data_arr=data.split(",")
                temp=round(float(data_arr[1]),2)
                humid=round(float(data_arr[2]),2)
                return {"chip":data_arr[0],"Temperature":temp,"Humidity":humid}
            except:
                print(data)
                return  {"chip":None,"Temperature":None,"Humidity":None}
if __name__=="__main__":
    pi=rpi("/dev/ttyUSB2")
    while True:
        print(pi.get_data())
