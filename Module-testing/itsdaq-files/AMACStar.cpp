#include "AMACStar.h"

#include "stdll/TimeSeries.h"
#include "Influx.h"

#ifdef ITSDAQ_BUILD_CHECK
#include "runtimeCommon.h"
#endif

// initialise all the AMACs, if channels is empty it assumes sequential channels...
void AMACStar_initialiseAMACs(std::vector < int > addresses, std::vector < int > channels)
{
  AMAC_common_initialiseAMACs(addresses, channels, POWERCHIP_AMAC_STAR);
}

void AMACStar_configureAMACs(int enableDCDC)
{
  if(addresses_final.empty()) {
    std::cout << "Called AMACStar_configureAMACs with no AMACs configured\n";
    return;
  }

  for (size_t i = 0; i < addresses_final.size(); i++)
    {
      AMACStar_setID(i);
      AMACStar_setID(i);

      AMACStar_writeConfiguration(i,enableDCDC);
    }
}

void AMACStar_internalCalibrations()
{
  if(addresses_final.empty()) {
    std::cout << "Called AMACStar_internalCalibrations with no AMACs configured\n";
    return;
  }

  // then do offsets for all the AMACs
  for (size_t i = 0; i < addresses_final.size(); i++) AMACStar_calibrateOffsets(i,true);

  // then do NTC refs
  for (size_t i = 0; i < addresses_final.size(); i++)
    for (int j = 0; j < 3; j++)
      AMACStar_NTCref(i,j,true);
}

void AMACStar_calibrateOffsets(int amac, bool updateOffset)
{
  if(amac < 0) {
    std::cout << "Can't call AMACStar_calibrateOffsets for all AMACs\n";
    return;
  } else if(amac >= addresses_final.size()) {
    std::cout << "Called AMACStar_calibrateOffsets for AMAC " << amac << " of " << addresses_final.size() << "\n";
    return;
  }

  for (int i = 0; i <= 15; i++)
    {
      AMACStar_zeroOffset(amac,i,updateOffset);
    }
}

void AMACStar_zeroOffset(int amac, int AMchan, bool updateOffset)
{
  if(amac < 0) {
    std::cout << "Can't call AMACStar_zeroOffset for all AMACs\n";
    return;
  } else if(amac >= addresses_final.size()) {
    std::cout << "Called AMACStar_zeroOffset for AMAC " << amac << " of " << addresses_final.size() << "\n";
    return;
  }

  // get current state of reg 44
  unsigned long int reg44 = AMACStar_readReg(amac, 44, false).at(0);

  // switch zero offset on
  unsigned long int reg44_new = reg44 + (1 << 8);


  // write registers
  AMACStar_writeReg(amac, 44, reg44_new);

  e->Sleep(1);

  // read AM
  int reading = AMACStar_readAM(amac, AMchan, 0).at(0);

  printf("AM%d\t= %d\n",AMchan,reading);  
  if (updateOffset) AM_offsets_final.at(amac).at(AMchan) = reading;

  // return registers
  AMACStar_writeReg(amac, 44, reg44);
}

void AMACStar_NTCref(int amac, int NTC, bool updateVref)
{
  if(amac < 0) {
    std::cout << "Can't call AMACStar_NTCref for all AMACs\n";
    return;
  } else if(amac >= addresses_final.size()) {
    std::cout << "Called AMACStar_NTCref for AMAC " << amac << " of " << addresses_final.size() << "\n";
    return;
  }

  // get current state of reg 51
  unsigned long int reg51 = AMACStar_readReg(amac, 51, false).at(0);

  // switch calibration on
  unsigned long int reg51_new = reg51 - (1 << (3 + NTC*8));

  // write register
  AMACStar_writeReg(amac, 51, reg51_new);

  e->Sleep(11);

  // read AM
  int ntc_chan = AMACSTAR_AM_NTCX + NTC;
  float reading = AMACStar_readAM_calibrated(amac, ntc_chan, 0).at(0);

  printf("NTC%d Vref\t=%f\n",NTC,reading);

  if (updateVref) NTC_vrefs_final.at(amac).at(NTC) = reading;

  // return registers
  AMACStar_writeReg(amac, 51, reg51);
}

void AMACStar_setID(int amac)
{
  AMAC_common_setID(amac);
}

void AMACStar_writeConfiguration(int amac, int enableDCDC)
{
  if(amac >= addresses_final.size()) {
    std::cout << "Called AMACStar_writeConfiguration for AMAC " << amac << " of " << addresses_final.size() << "\n";
    return;
  }

  if(load_amac_type != POWERCHIP_AMAC_STAR) {
    std::cout << "Warning AMACStar_writeConfiguration: load_amac_type not set to POWERCHIP_AMAC_STAR\n";
  }

  // THIS IS THE POINT WHERE YOU WOULD READ E-FUSE AND LOAD SPECIFIC CONFIGURATIONS FROM THE DATABASE!!!

  AMACStar_writeReg(amac,  40  ,  HV_settings_final.at(amac)); // HV enabled by default but can be changed if necessary
  if(enableDCDC == 1){
    AMACStar_writeReg(amac,  41  ,  0x00000001); // DCDC enabled
  }else if(enableDCDC == 0){
    AMACStar_writeReg(amac,  41  ,  0x00000000); // DCDC disabled
  }
  AMACStar_writeReg(amac,  42  ,  0x00000000); // no interlock or warnings enabled
  AMACStar_writeReg(amac,  43  ,  0x00010100); // HCC resets set (takes HCC out of reset
  AMACStar_writeReg(amac,  44  ,  0x00000001); // AM enabled 
  AMACStar_writeReg(amac,  45  ,  0x00000100); // require DCDC PGOOD, but do not power from DCDC
  AMACStar_writeReg(amac,  46  ,  BG_settings_final.at(amac)); 
  AMACStar_writeReg(amac,  47  ,  0x00000000); // default
  AMACStar_writeReg(amac,  48  ,  0x00000000); // shunt values at zero
  AMACStar_writeReg(amac,  49  ,  0x0000000D); // DAC bias at default
  AMACStar_writeReg(amac,  50  ,  0x00000404); // default 
  AMACStar_writeReg(amac,  51  ,  0x03040404); // default
  AMACStar_writeReg(amac,  52  ,  0x00401500); // default
}

void AMACStar_writeReg(int amac, unsigned long int reg, unsigned long int content)
{
  AMAC_common_writeReg(amac, reg, content);
}

std::vector < unsigned long int > AMACStar_readReg(int amac, unsigned long int regaddr, bool printReg)
{
  return AMAC_common_readReg(amac, regaddr, printReg);
}

void AMACStar_dumpRange(int amac, int reg_addr, int reg_count)
{
  AMAC_common_dumpRange(amac, reg_addr, reg_count);
}

std::vector < int > AMACStar_readAM(int amac, int AMchan, int MUX)
{
  int AMAC_register = 0; int AM_count = 0;
  if (AMchan < 3)
    {
      AMAC_register = 10;
      AM_count = AMchan;
    }
  else if (AMchan < 6)
    {
      AMAC_register = 11;
      AM_count = AMchan - 3;
    }
  else if (AMchan < 9)
    {
      AMAC_register = 12;
      AM_count = AMchan - 6;
    }
  else if (AMchan < 12)
    {
      AMAC_register = 13;
      AM_count = AMchan - 9;
    }
  else if (AMchan < 14)
    {
      AMAC_register = 14;
      AM_count = AMchan - 12;
    }
  else if (AMchan < 16)
    {
      AMAC_register = 15;
      AM_count = AMchan - 14;
    }
  else {
    std::cout << "AMACStar_readAM: Bad AMchan " << AMchan << " only 16 options\n";
    return {};
  }

  // set MUX correctly
  if (AMchan == 12 || AMchan == 13 || AMchan == 14 || AMchan == 15)
    {
      int shift = 0;
      if (AMchan == 12)
	{
	  shift = 4;
	}
      if (AMchan == 13) 
	{
	  shift = 8;
	}
      if (AMchan == 14) 
	{
	  shift = 12;
	}
      if (AMchan == 15) 
	{
	  shift = 16;
	}

      unsigned long int reg47 = AMACStar_readReg(amac, 47, false).at(0);
      // All width 3
      unsigned long int mask = 0xfffffff8;

      unsigned long int newVal = MUX;
      // For AMchan 12, VDDLR_lo has its own mux bit. 
      // Effectively valid MUX codes as 0,1,2 and 4.  Wierdly 3 is equivalent to 0.
      // So we'll wrap this up in software and replace 3 by 4...
      if (AMchan == 12 && MUX > 3)
	{
	  newVal = 4;  // second level mux
	}
      mask = (mask << shift) | (mask >> (32-shift)); // rotate left
      newVal = newVal << shift;

      reg47 = (reg47 & mask) | newVal;

      AMACStar_writeReg(amac,47,reg47);
    }
 
  e->Sleep(1);

  std::vector < int > returnVal = {};

  std::vector < unsigned long int > values = AMACStar_readReg(amac, AMAC_register, false);

  for (size_t loop = 0; loop < values.size(); loop++)
    {
      unsigned long int val = values.at(loop);
      
      unsigned long int mask = 0x3ff;
      mask = mask << (10 * AM_count);
      
      val = (val & mask) >> (10 * AM_count);

      returnVal.push_back(val);
   }
  return returnVal;
}

std::vector < float > AMACStar_readAM_calibrated(int amac, int AMchan, int MUX)
{
  std::vector < int > readings = AMACStar_readAM(amac,AMchan,MUX);

  std::vector < float > returnData = {};

  size_t loopStart, loopEnd;
  if(amac == -1) {
    loopStart = 0;
    loopEnd = addresses_final.size();
  } else {
    loopStart = amac;
    loopEnd = loopStart + 1;
  }

  for (size_t amac_index = loopStart; amac_index < loopEnd; amac_index++)
    {
      // If reading single AMAC readings is size 1
      int reading = readings.at(amac_index-loopStart);

      float CALfactor = 1.0;

      if (AMchan == 0) CALfactor = 2;
      if (AMchan == 1) CALfactor = 15;
      if (AMchan == AMACSTAR_AM_PTAT) CALfactor = 2;
      if (AMchan == 13 && MUX == 1) CALfactor = 1.5;
      if (AMchan == 14 && MUX == 0) CALfactor = 4;

      auto offset = AM_offsets_final.at(amac_index).at(AMchan);
      auto slope = AM_slopes_final.at(amac_index);

      returnData.push_back(CALfactor * (reading - offset) * slope);
    }

  return returnData;
}

std::vector < float > AMACStar_readNTC(int amac, int NTC, bool doAutoRange) // NTC0 = HX, NTC1 = HY, NTC2 = PB
{

  std::vector < float > returnT = {};

  int loopStart, loopEnd;
  if ( amac == -1 )
    {
      loopStart = 0;
      loopEnd = addresses_final.size();
    }
  else
    {
      if(amac >= addresses_final.size()) {
        std::cout << "Called AMACStar_readNTC for AMAC " << amac << " of " << addresses_final.size() << "\n";
        return returnT;
      }

      loopStart = amac;
      loopEnd = loopStart + 1;
    }

  int ntc_chan = AMACSTAR_AM_NTCX + NTC;

  for (int loop = loopStart; loop < loopEnd; loop++)
    {
      
      // get current state of reg 51
      unsigned long int reg51 = AMACStar_readReg(loop, 51, false).at(0);

      // Is this range appropriate?
      int direction = 0;
      unsigned long int range = (reg51>>(8*NTC)) & 0x07;
      unsigned long int reading = AMACStar_readAM(loop,ntc_chan,0).at(0);
      if(doAutoRange == true){
	if((reading > 1020)&&(range < 7)){
	  direction = 1;
	  range++;
	}
	if((reading < 100)&&(range > 0)){
	  direction = -1;
	  range--;
	}
      }

      while( direction!=0 ) {
	printf("Amac %d NTC %d autorange direction %d before adjustment %08lx\n",loop,NTC,direction,reg51);

	// set the range
	if(NTC==0) reg51 &= 0xfffffff8;
	if(NTC==1) reg51 &= 0xfffff8ff;
	if(NTC==2) reg51 &= 0xfff8ffff;
	reg51 |= (range << (8*NTC));
	printf("Amac %d NTC %d autorange direction %d after adjustment %08lx\n",loop,NTC,direction,reg51);
	AMACStar_writeReg(loop, 51, reg51);
	e->Sleep(1);

	// read current setting in DAC counts
	reading = AMACStar_readAM(loop,ntc_chan,0).at(0);
	if(direction == 1){
	  if(range==7) direction = 0;
	  if(reading > 900){
	    range++;
	  }else{
	    direction = 0;
	  }
	}
	if(direction == -1){
	  if(range==0) direction = 0;
	  if (reading < 500){
	    range--;
	  }else{
	    direction = 0;
	  }
	} 
      } 

      float Vntc = AMACStar_readAM_calibrated(loop,ntc_chan,0).at(0);


      // get switch configuration
      int S0 = ((reg51 >> (8 * NTC)) >> 0) & 1;
      int S1 = ((reg51 >> (8 * NTC)) >> 1) & 1;
      int S2 = ((reg51 >> (8 * NTC)) >> 2) & 1;

      // resistors
      float R = 200e3;
      float R0 = 133e3;
      float R1 = 50e3;
      float R2 = 22.22e3;

      // calculate Rset
      float Rset = 1/(1/R+S0/R0+S1/R1+S2/R2);

      // calibrate NTC reading here
      float Acl = Vntc/NTC_vrefs_final.at(loop).at(NTC);

      float Rntc = Rset/(Acl-1);

      // remove trace resistance
      if(NTC==0) Rntc -= 480;
      if(NTC==1) Rntc -= 550;	
      if(NTC==2) Rntc -= 500;

      float r25;
      float b;

      if(NTC==2){ // powerboard
        r25 = R25_PB_final;
        b = B_PB_final;
      } else {
        r25 = R25_final;
        b = B_final;
      }

      float T = 1 / ( log( Rntc / r25 ) / b + 1 / T25_final ) - 273.15;

      returnT.push_back(T);
    }

  return returnT;
}

std::vector < float > AMACStar_readIDET(int amac, unsigned long int nReadings, bool printValue, bool doAutoRange, bool returnAll)
{
  std::vector < float > returnI = {};

  int loopStart, loopEnd;
  if ( amac == -1 )
    {
      loopStart = 0;
      loopEnd = addresses_final.size();
    }
  else
    {
      if(amac >= addresses_final.size()) {
        std::cout << "Called AMACStar_readIDET for AMAC " << amac << " of " << addresses_final.size() << "\n";
        return returnI;
      }

      loopStart = amac;
      loopEnd = loopStart + 1;
    }

  int idet_chan = AMACSTAR_AM_HVRET;

  for (int loop = loopStart; loop < loopEnd; loop++)
    {
      
      // get current state of reg 50 and read the AM
      unsigned long int reg50 = AMACStar_readReg(loop, 50, false).at(0);

      // Is this range appropriate?
      int direction = 0;
      unsigned long int range = (reg50>>16) & 0x0f;
      unsigned long int reading = AMACStar_readAM(loop,idet_chan,0).at(0);
      if(doAutoRange == true){
	if((reading > 950)&&(range < 12)){
	  // shift left 
	  direction = 1;
	  if(range==0) range=1;
	  else if (range == 8) range=12;
	  else range = (range << 1) & 0x0f;
	}
	if((reading < 200)&&(range > 0)){
	  // shift right
	  direction = -1;
	  if (range == 12) range=8;
	  else range = (range >> 1) & 0x0f;
	}
      }

      while( direction!=0 ) {
	printf("Amac %d IDET autorange direction %d before adjustment %08lx\n",loop,direction,reg50);

	// set the range
	reg50 = ( reg50 & 0xff00ffff) | (range << 16);
	AMACStar_writeReg(loop, 50, reg50);
	e->Sleep(1);

	// read current setting in DAC counts
	reading = AMACStar_readAM(loop,idet_chan,0).at(0);
	if(direction == 1){
	  if(range==12) direction = 0;
	  if(reading > 800){
	    // shift left
	    if (range == 8) range=12;
	    else range = (range << 1) & 0x0f;
	  }else{
	    direction = 0;
	  }
	}
	if(direction == -1){
	  if(range==0) direction = 0;
	  if (reading < 200){
	    // shift right
	    if (range == 12) range=8;
	    else range = (range >> 1) & 0x0f;
	  }else{
	    direction = 0;
	  }
	} 
      } 

      // get switch configuration
      int S0 = (reg50 >> 16) & 1;
      int S1 = (reg50 >> 17) & 1;
      int S2 = (reg50 >> 18) & 1;
      int S3 = (reg50 >> 19) & 1;

      // resistors
      float R = 200e3;
      float R0 = 17.5e3;
      float R1 = 1.8e3;
      float R2 = 300;
      float R3 = 120;

      // calculate Rset
      float Rset = 1/(1/R+S0/R0+S1/R1+S2/R2+S3/R3);

      // returns a number in mV
      std::vector<float> Vids;
      Vids.clear();  
      for(size_t i =0; i<nReadings; i++){
	Vids.push_back(AMACStar_readAM_calibrated(loop,idet_chan,0).at(0));
      }

      float Vmean = 0;
      for(size_t i=0; i<Vids.size(); i++){
	Vmean += Vids.at(i);
      } 
      if(Vids.size()>0){
	Vmean /= (float)Vids.size();
      }

      float Vrms = 0;
      for(size_t i=0; i<Vids.size(); i++){
	Vrms += (Vids.at(i)-Vmean)* (Vids.at(i)-Vmean);
      }
      if(Vids.size()>0){
	float sr = TMath::Sqrt(Vrms);
	Vrms = sr / (float)Vids.size();
      }

      float Imean = Vmean/(1000*Rset);
      float Irms = Vrms/(1000*Rset);

      if (printValue) printf("%d range %lu mean %3.1fmV rms %3.1fmV mean %1.2eA rms %1.2eA\n",
			     loop,range,Vmean,Vrms,Imean,Irms);

      

      // sensor current readings
      sensorIrange_final.at(loop)=range;
      sensorImeanmV_final.at(loop)=Vmean;
      sensorIrmsmV_final.at(loop)=Vrms;
      sensorImeanA_final.at(loop)=Imean;
      sensorIrmsA_final.at(loop)=Irms;
   
      returnI.push_back(Imean * 1e9);
      if(returnAll){
      	returnI.push_back(Irms * 1e9);
      	returnI.push_back(Vmean);
      	returnI.push_back(Vrms);
      	returnI.push_back(range);
      }
    }

  return returnI; // return in nA
}

std::vector < float > AMACStar_readPTAT(int amac)
{
  size_t loopStart, loopEnd;
  if ( amac == -1 ) {
    loopStart = 0;
    loopEnd = addresses_final.size();
  } else {
    if(amac >= addresses_final.size()) {
      std::cout << "Called AMACStar_readPTAT for AMAC " << amac << " of " << addresses_final.size() << "\n";
      return {};
    }

    loopStart = amac;
    loopEnd = loopStart + 1;
  }

  int ptat_chan = AMACSTAR_AM_PTAT;

  // From: zero = <Vptat> - ptat_constant*NTCpb_mean;
  // T = (Vptat - zero)/ptat_constant

  float ptat_constant = 4.85;

  std::vector < float > ptat_t{};

  for (size_t loop = loopStart; loop < loopEnd; loop++) {
    // Read the ptat value in mV
    float Vptat = AMACStar_readAM_calibrated(loop,ptat_chan,0)[0];

    auto ptat_zero = ptat_zero_ref_final[loop];

    auto t = (Vptat - ptat_zero) / ptat_constant;

    ptat_t.push_back(t);
  }

  return ptat_t;
}

std::vector < float > AMACStar_readPTATzero(int amac, int nReadings, bool update_ref)
{
  if(nReadings <= 0) {
    std::cout << "Need at least one reading to find PTAT zero\n";
    return {};
  }

  // Avoid turning off bpol, instead print warning to the user
  auto reg_41 = AMACStar_readReg(amac, 41, false);

  if(reg_41.empty()) {
    std::cout << "Read reg failed PTAT zero (DCDC check)\n";
    return {};
  }

  for(auto &reg: reg_41) {
    if(reg & 1) {
      std::cout << "At least one AMAC is on\n";
      std::cout << "Please turn off before reading PTAT zero\n";
      return {};
    }
  }

  int loopStart, loopEnd;
  if ( amac == -1 )
    {
      loopStart = 0;
      loopEnd = addresses_final.size();
    }
  else
    {
      if(amac >= addresses_final.size()) {
        std::cout << "Called AMACStar_readPTAT for AMAC " << amac << " of " << addresses_final.size() << "\n";
        return {};
      }

      loopStart = amac;
      loopEnd = loopStart + 1;
    }

  int ptat_chan = AMACSTAR_AM_PTAT;
  int ntc_pb = 2;

  float ptat_constant = 4.85;

  std::vector < float > ptat0 = {};

  // wait for ptat value to stabilize (temperature after DCDC is turned off)
  e->Sleep(1000);

  for (int loop = loopStart; loop < loopEnd; loop++)
    {
      // measure the ptat value in mV
      std::vector < float > Vptat{};
      float Vptat_mean = 0;
      for(size_t i =0; i<nReadings; i++){
        float ptat_reading = AMACStar_readAM_calibrated(loop,ptat_chan,0)[0];
        Vptat_mean += ptat_reading;
      }
      Vptat_mean /= (float)nReadings;

      float NTCpb_mean =0;
      for(size_t i =0; i<nReadings; i++){
        float ntc_reading = AMACStar_readNTC(loop,ntc_pb,1)[0];
        NTCpb_mean += ntc_reading;
      }

      NTCpb_mean /= (float)nReadings;

      float zero = Vptat_mean - ptat_constant*NTCpb_mean;

      if(update_ref) {
        // Update calibration
        ptat_zero_ref_final[loop] = zero;
      }

      ptat0.push_back(zero);
    }

  return ptat0;
}

void AMACStar_DCDC(int amac, int state)
{
  if(state == 1){
    AMACStar_writeReg(amac,  41  ,  0x00000001); // DCDC enabled
  }else if(state == 0){
    AMACStar_writeReg(amac,  41  ,  0x00000000); // DCDC disabled
  }
}

void AMACStar_calScan(bool updateCal,
                          std::unique_ptr<AMACv2_Calibration> set_cal)
{
  if(addresses_final.empty()) {
    std::cout << "Called AMACStar_calScan with no AMACs configured\n";
    return;
  }

  set_cal->Init();


  AMACgraphs.clear();
  for (size_t i = 0 ; i < addresses_final.size(); i++) {
    AMACgraphs.push_back( new TGraph() );
  }

  int cal_chan = AMACSTAR_AM_CAL;

  for (int i = 0; i <= 100; i++) {
    float setVoltage = i / 100.;

    set_cal->SetVoltage(setVoltage);

    // correct for step size but it's a tiny effect!!!
    double step = 1.5 / (double) 0x0fff;
    uint16_t iA = (uint16_t) (setVoltage/step);
    setVoltage = step * iA;

    float milliV = setVoltage*1000.0;
    std::cout << milliV << "\t";

    for (size_t amac = 0; amac < addresses_final.size(); amac++) {
      // not sure why this is needed, but it is!
      if (i == 0) {
        AMACStar_readAM(amac,cal_chan,1);
        e->Sleep(100);
      }

      int reading = AMACStar_readAM(amac,cal_chan,1)[0];
      std::cout << reading << "\t";

      AMACgraphs[amac]->SetPoint(i,reading,milliV);
    }
    std::cout << "\n";
  }

  set_cal->Shutdown();

  // now let's fit the graphs just up to 500mV right now)
  TF1 * fit = new TF1("fit","[0]*x+[1]",0,500);
  for (size_t amac = 0 ; amac < addresses_final.size(); amac++) {
    fit->SetParameter(0,AM_slopes_final[amac]);
    fit->SetParameter(1,AM_intercepts_final[amac]);
    AMACgraphs[amac]->Fit(fit,"NR");

    if (updateCal) {
      AM_slopes_final[amac] = fit->GetParameter(0);
      AM_intercepts_final[amac] = fit->GetParameter(1);
    }
  }
}

void BERT_rand_Star(int amacid, uint64_t nEvents, bool endAtFail, bool slow, int nAttempts, unsigned long long int pattern){
  BERT_rand_common(amacid, nEvents, endAtFail, slow, nAttempts, pattern, 100); // use an interlock threshold register
}

void AMACStar_removeAMAC(int i){
  AMAC_common_removeAMAC(i);
}

void AMACStar_reportADCs(std::string fname) {
  std::ofstream os(fname.c_str(), std::ios::app);
  AMACStar_reportADCs(-1, os);
  os.close();
}

void AMACStar_reportADCs(int amac_id) {
  AMACStar_reportADCs(amac_id, std::cout);
}

void AMACStar_reportADCs(int amac_id, std::ostream &os) {
  size_t amac_count = 1;
  if(amac_id == -1) {
    amac_count = addresses_final.size();
  }

  if(addresses_final.empty()) {
    os << "Called AMACStar_reportADCs with no AMAC configuration\n";
    return;
  }

  static const std::string adc_names[] =
    {"VDCDC", "DCDCin", "NTCx", "NTCy", "NTCpb",
     "CTAT", "Cur1V5", "HVret", "PTAT",
     "Hrefx", "Hrefy", "CAL", "VDDLR",
     "Cur10V", "VDDHI", "output"   };

  typedef std::pair<int, int> AMMuxKey;

  // For each ADC/MUX we have data for each AMAC
  std::map<AMMuxKey, std::vector <float>> dataMap{};
  int nbad = 0;

  std::map <AMMuxKey, std::string> fieldNames{};

  for(int adc=0;adc<16; adc++) {
    int mux_count = 1;
    if(adc == 12 ) {
      mux_count = 4;
    } else if(adc == 13 || adc == 14 || adc == 15) {
      mux_count = 5;
    } else if(adc == AMACSTAR_AM_NTCX
              || adc == AMACSTAR_AM_NTCY
              || adc == AMACSTAR_AM_NTCPB
              || adc == AMACSTAR_AM_HVRET
              || adc == AMACSTAR_AM_PTAT) {
      // Add virtual second mux for calibrated readout
      mux_count = 2;
    }

    for(int mux=0; mux<mux_count; mux++) {
      AMMuxKey key{adc, mux};
      std::vector <float> data;

      if(mux_count == 2 && mux == 1) {
        switch(adc) {
        case AMACSTAR_AM_NTCX:
        case AMACSTAR_AM_NTCY:
        case AMACSTAR_AM_NTCPB:
          data = AMACStar_readNTC(amac_id, adc-AMACSTAR_AM_NTCX, true);
          break;
        case AMACSTAR_AM_HVRET:
          data = AMACStar_readIDET(amac_id, 10, false, true);
          break;
        case AMACSTAR_AM_PTAT:
          data = AMACStar_readPTAT(amac_id);
          break;
        default:
          std::cout << "Unexpected ADC/MUX " << adc << " " << mux << "\n";
          break;
        }
      } else {
        data = AMACStar_readAM_calibrated(amac_id, adc, mux);
      }
      switch(adc) {
      default:
        break;
      }

      for(size_t i=0; i<data.size(); i++){
        dataMap[key].push_back(data[i]);
      }

      std::string name = adc_names[adc];

      if(mux_count != 1) {
        if(adc == AMACSTAR_AM_NTCX
           || adc == AMACSTAR_AM_NTCY
           || adc == AMACSTAR_AM_NTCPB
           || adc == AMACSTAR_AM_HVRET
           || adc == AMACSTAR_AM_PTAT) {
          if(mux == 0) {
            name += "_V";
          }
        } else if(adc == 12 && mux == 0) {
          name = "VDDLRlow";
        } else if(adc == 12 && mux == 1) {
          name = "AM900BG";
        } else if(adc == 12 && mux == 2) {
          name = "AM600BG";
        } else if(adc == 12 && mux == 3) {
          name = "VDDLRhigh";
        } else if(adc == 13 && mux == 0) {
     //unchanged
        } else if(adc == 13 && mux == 1) {
          name = "VREG";
        } else if(adc == 13 && mux == 2) {
          name = "AMref";
        } else if(adc == 13 && mux == 3) {
          name = "CHIPGND";
        } else if(adc == 13 && mux == 4) {
          name = "HGND";
        } else if(adc == 14 && mux == 0) {
     //unchanged
        } else if(adc == 14 && mux == 1) {
          name = "DCDCinLow";
        } else if(adc == 14 && mux == 2) {
          name = "DCDCinHigh";
        } else if(adc == 14 && mux == 3) {
          name = "DCDCoutLow";
        } else if(adc == 14 && mux == 4) {
          name = "DCDCoutHigh";
        } else if(adc == 15 && mux == 0) {
          name = "BG600";
        } else if(adc == 15 && mux == 1) {
          name = "CALx";
        } else if(adc == 15 && mux == 2) {
          name = "CALy";
        } else if(adc == 15 && mux == 3) {
          name = "Shuntx";
        } else if(adc == 15 && mux == 4) {
          name = "Shunty";
        } else {
          // I think all cases are covered, but change name if not
          name += '_';
          name += (mux + 'a');
        }
      }
      fieldNames[key] = name;

      os << std::setw(10) << name << ":";
      if(data.empty()) {
        os << "   Error in read";
      } else {
        for(auto &d: data) {
          // eg Cur10V: 10337.6
          os << " " << std::setw(7) << d;
        }
        if(data.size() != amac_count) {
          os << " Error in read";
          nbad++;
        }
      }
      os << "\n";
    }
  }

  if(nbad>0){
    os << "AMACStar_reportADCs: read error, skipping InfluxDB\n";
    return;
  }

  // Now we loop over dataMap AMAC by AMAC, in order to format data for influx
  TimeSeries amacTS;
  TagData    amacTD;
  FieldData  amacFD;

  for (int amac = 0; amac < amac_count; amac++){
    for(auto hi=dataMap.begin(); hi!=dataMap.end(); hi++) {
      auto &mux_key = hi->first;
      auto &data = hi->second;

      auto datum = data[amac];

      if(!isnan(datum)) {
        amacFD[fieldNames[mux_key]] = FieldType(datum);

        if(!Amac_Det_Map_final.empty()) {
          // Find corresponding TModule (one of them) to save DCS info
          int det_id = Amac_Det_Map_final[amac];
          e->m[det_id]->DCS->Record("AMAC_" + fieldNames[mux_key], datum);
        }
      }
    }

    int addr = addresses_final[amac];
    if(amac_count==1){
      addr = addresses_final[amac_id];
    }
    amacTD["Mapping"] = Form("Port%02d.Addr%02d",channels_final[amac],addr);
    
    amacTS.Record("DCS_AMAC", amacTD, amacFD);
  }

  std::string data = amacTS.GetLineProtocolData();
  SendDataToInflux(data);
}

void AMACStar_plotcal(){
  AMAC_common_plotcal();
}

void AMACStar_reportConfig() {
  if(load_amac_type != POWERCHIP_AMAC_STAR) {
    std::cout << "Warning: AMACStar_reportConfig called when star not configured\n";
  }
  AMAC_common_reportConfig();
}

void AMACStar_startupStep(int amac, int step){

  switch(step){
    case 0: // DC-DC off
      std::cout << "Step 0: DC-DC OFF" << std::endl;
      AMACStar_DCDC(amac,0);
      break;
    case 1: // DC-DC on - but hold HCC and ABC in LPM
      std::cout << "Step 1: DC-DC ON, all chip in LPM" << std::endl;
      AMACStar_writeReg(amac,  40  ,  0x07070000); // set all LPM high
      AMACStar_writeReg(amac,  43  ,  0x00000000); // HCC reset cleared (puts HCC into reset)
      AMACStar_writeReg(amac,  41  ,  0x00000001); // DCDC enabled
      break;
    case 2: // release HCC LPM
      std::cout << "Step 2: DC-DC ON, release HCC LPM" << std::endl;
      AMACStar_writeReg(amac,  40  ,  0x07071100); // set HCC LPM high
      AMACStar_writeReg(amac,  43  ,  0x00000000); // HCC reset cleared (puts HCC into reset)
      AMACStar_writeReg(amac,  41  ,  0x00000001); // DCDC enabled
      break;
    case 3: // release ABC0 LPM
      std::cout << "Step 3: DC-DC ON, release ABC0 LPM" << std::endl;
      AMACStar_writeReg(amac,  40  ,  0x07073300); // set HCC + ABC0 LPM high
      AMACStar_writeReg(amac,  43  ,  0x00000000); // HCC reset cleared (puts HCC into reset)
      AMACStar_writeReg(amac,  41  ,  0x00000001); // DCDC enabled
      break;
    case 4: // release ABC1 LPM
      std::cout << "Step 4: DC-DC ON, release ABC1 LPM" << std::endl;
      AMACStar_writeReg(amac,  40  ,  0x07077700); // set HCC + ABC0 + ABC1 LPM high
      AMACStar_writeReg(amac,  43  ,  0x00000000); // HCC reset cleared (puts HCC into reset)
      AMACStar_writeReg(amac,  41  ,  0x00000001); // DCDC enabled
      break;
    case 5: // release HCC RESET
      std::cout << "Step 5: DC-DC ON, release HCC reset" << std::endl;
      AMACStar_writeReg(amac,  40  ,  0x07077700); // set HCC + ABC0 + ABC1 LPM high
      AMACStar_writeReg(amac,  43  ,  0x00010100); // HCC resets set (takes HCC out of reset)
      AMACStar_writeReg(amac,  41  ,  0x00000001); // DCDC enabled
      break;
    default: // configure HCC & ABC
      std::cout << "Step 6: Configure STUFF" << std::endl;
      AMACStar_writeReg(amac,  40  ,  0x07077700); // set HCC + ABC0 + ABC1 LPM high
      AMACStar_writeReg(amac,  43  ,  0x00010100); // HCC resets set (takes HCC out of reset)
      AMACStar_writeReg(amac,  41  ,  0x00000001); // DCDC enabled
      e->ExecuteConfigs();
      break;
   }
}

void AMACStar_tuneRampGain(int amac, int target = 500){

//  set_cal->Init();
//  float setVoltage = (float) target / 100.;
//  set_cal->SetVoltage(setVoltage);

  const int slope_reg = 47;

  auto reg47 = AMACStar_readReg(amac, slope_reg, false);

  std::vector<int> best(reg47.size(), -1);
  std::vector<int> delta(reg47.size(), 99999);

  auto writeSlope = [&amac, &reg47](int sl, int this_id) {
    if(amac == -1 && this_id == -1) {
      for(int a=0; a<reg47.size(); a++) {
        uint32_t regVal = reg47[a] & 0xffffff;
        AMACStar_writeReg(a, slope_reg, regVal | (sl<<24));
      }
    } else if(amac == -1) {
      // Write specific amac
      uint32_t regVal = reg47[this_id] & 0xffffff;
      AMACStar_writeReg(this_id, slope_reg, regVal | (sl<<24));
    } else {
      uint32_t regVal = reg47[0] & 0xffffff;
      AMACStar_writeReg(amac, slope_reg, regVal | (sl<<24));
    }
  };

  for(int i=0; i<16; i++){
    writeSlope(i, amac);
    e->Sleep(50);

    std::vector<int> slice = AMACStar_readAM(amac,AMACSTAR_AM_CAL,0);

    for(size_t j=0; j<slice.size(); j++){
      int newDelta = slice[j] - target;
      if(newDelta <0) newDelta *= -1; 
      if(newDelta < delta[j]){
        delta[j] = newDelta;
        best[j] = i;
      }
    }

  }

  if(amac == -1){
    for(size_t j=0; j<delta.size(); j++){
      if(best[j]>-1){
        std::cout << "AMAC " << j << " best " << best[j] << " delta " << delta[j] << std::endl;
        writeSlope(best[j], j);
      }
    }
  }else{
    if(best[0]>-1){
      std::cout << "AMAC " << amac << " best " << best[0] << " delta " << delta[0] << std::endl;
      writeSlope(best[0], amac);
    }
  }
}


std::vector<float> AMACStar_readLinPolCurr(int amacid){
   // Output
   std::vector<float> meas;
   
   // Read AMAC AM channel 12aa (VDDLRlo)
   std::vector<int> VDDLRlo = AMACStar_readAM(amacid, 12, 0);
   
   // Read AMAC AM channel 12ab (VDDLRhi)
   std::vector<int> VDDLRhi = AMACStar_readAM(amacid, 12, 4);

   // Compute current
   float calibFactor = 0.58;
   if (amacid == -1){
     for (int i=0; i<addresses_final.size(); i++){
       float linPolCurr = 1/calibFactor * (AM_slopes_final[i]*(VDDLRhi[i]-VDDLRlo[i])/1.0); //1.0 as if it were a 1Ohm resistor to go from mV to mA.
       meas.push_back(linPolCurr);
     }
   } else{
     if (amacid < addresses_final.size()){
       float linPolCurr = 1/calibFactor * (AM_slopes_final[0]*(VDDLRhi[0]-VDDLRlo[0])/1.0); //1.0 as if it were a 1Ohm resistor to go from mV to mA.
       meas.push_back(linPolCurr);
     }
   }

   return meas;
}

// Parameters for tuning current mirrors
// All route to ADC channel 14
// Input Current TPlo 14b
// Input Current TPhi 14c
// Output Current TPlo 14d
// Output Current TPhi 14e
//
// Calibration parameters in register 52
// Input P 0-3
// Input N 4-7
// Input offset 8-11
// Output P 16-17
// Output N 18-19
// Output offset 20-23
//
// Method: Gain
//   for each(p,n){
//     read both TP
//   }
//   choose (p,n) with smallest positive difference (hi-lo)
//
// Method: Offset
//   for chosen (p,n) set offset to give >=100 (pedestal subtracted) ADC counts for **ZERO CURRENT DRAW**
//   Zero current draw MAY be possible using bits Zin (15) and Zout (24) of register 52

void AMACStar_tuneCurrentMirror(int amac, bool outNotIn, int setZ = 1, int target = 100){

  // Read and store initial values such that other settings are not changed
  
  const int tune_reg = 52;
  auto reg52 = AMACStar_readReg(amac, tune_reg, false);

  auto writePNOZ = [&amac, &reg52](bool outNotIn, int p, int n, int o, int z, int this_id) {
    uint32_t mask = 0;
    uint32_t bits = 0;
    if(outNotIn){
        mask = 0xff00ffff;
        bits |= (p<<16);
        bits |= (n<<18);
        bits |= (o<<20);
        bits |= (z<<24);
    }else{
        mask = 0xffffff00;
        bits |= (p<<0);
        bits |= (n<<4);
        bits |= (o<<8);
        bits |= (z<<15);
    }
 
    if(amac == -1 && this_id == -1) {
      for(size_t a=0; a<reg52.size(); a++) {
        uint32_t regVal = reg52[a] & mask;
        AMACStar_writeReg(a, tune_reg, regVal | bits);
      }
    } else if(amac == -1) {
      // Write specific amac
      uint32_t regVal = reg52[this_id] & mask;
      AMACStar_writeReg(this_id, tune_reg, regVal | bits);
    } else {
      uint32_t regVal = reg52[0] & mask;
      AMACStar_writeReg(amac, tune_reg, regVal | bits);
    }
  };


  int npn = 8;    // Number of adjustment points on each output current p or n TP
  int mlo = 1;    // mux setting for input current calibration TPlo
  int amchan = 6; // am channel for input current measurement
  if(outNotIn){
    npn = 4;      // Number of adjustment points on each output current p or n TP
    mlo = 3;      // mux setting for output current calibration TPlo
    amchan = 13;  // am channel for output current measurement
  }

  std::vector<int> bestp(reg52.size(), -1);
  std::vector<int> bestn(reg52.size(), -1);
  std::vector<int> delta(reg52.size(), 99999);

  // choose (p,n) with smallest positive difference (hi-lo)
  for(int p=0; p<npn; p++){
    for(int n=0; n<npn; n++){
      writePNOZ(outNotIn,p,n,0,setZ,amac);
      e->Sleep(50);

      std::vector<int> TPlo = AMACStar_readAM(amac,14,mlo+0);
      std::vector<int> TPhi = AMACStar_readAM(amac,14,mlo+1);
    
      for(size_t a=0; a<TPlo.size(); a++){
        int diff = TPhi[a] - TPlo[a];
        if( (diff>0) && (diff<delta[a]) ){
          bestp[a]=p;
          bestn[a]=n;
          delta[a]=diff;
        }
      } 
    }
  }

  std::vector<int> besto(reg52.size(), -1);
  std::vector<int> error(reg52.size(), 99999);

  // Choose offset which gives lowest (pedestal subtracted) offset greater than 100 counts.
  for(int o=0; o<16; o++){
    for(size_t a=0; a<reg52.size(); a++) {
      if(bestp[a]>-1){
        writePNOZ(outNotIn,bestp[a],bestn[a],o,setZ,a);
      }
    }
    e->Sleep(50);
  
    std::vector<int> offsets = AMACStar_readAM(amac,amchan,0);

    for(size_t a=0; a<offsets.size(); a++){
      if( (bestp[a]>-1) && (offsets[a]>=1000) && (offsets[a]<error[a]) ){
        besto[a]=o;
        error[a]=offsets[a];
      }
    }
  }

  // Configure each AMAC to the "best" settings
  for(size_t a=0; a<besto.size(); a++){
    if(besto[a]>-1){
      std::cout << " amac " << a << " best p " << bestp[a] << " best n " << bestn[a] << " delta " << delta[a];
      std::cout << " best o " << besto[a] << " offset " << error[a] << std::endl;
      writePNOZ(outNotIn,bestp[a],bestn[a],besto[a],0,a);
    }
  }
}
