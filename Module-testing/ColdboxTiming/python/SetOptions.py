"""
23 November 2022
Abraham Tishelman-Charny

The purpose of this python module is to set run options for the CharacterizeTiming module.
"""

# Imports 
import argparse 

# Create parser and get command line arguments 
parser = argparse.ArgumentParser()
parser.add_argument("--dryRun", action="store_true", help="To run without submission")
parser.add_argument("--PlotTimes", action="store_true", help="Plot time distributions") 
parser.add_argument("--PlotAverages", action="store_true", help="Plot averages") 
parser.add_argument("--dataDirec", type=str, default="data", help="Directory to get timing data from.") # directory for output files from process being run 
options = parser.parse_args()